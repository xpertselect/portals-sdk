<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/portals-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PortalsSdk;

use XpertSelect\PortalsSdk\Repositories\DatasetRepository;
use XpertSelect\PortalsSdk\Repositories\DistributionRepository;

/**
 * Class PortalsSdk.
 *
 * Sdk for interacting with the HTTP API of a XpertSelect Portals installation.
 */
class PortalsSdk
{
    /**
     * The repository for interacting with the dataset endpoints.
     */
    private ?DatasetRepository $datasetRepository;

    /**
     * The repository for interacting with the distribution endpoints.
     */
    private ?DistributionRepository $distributionRepository;

    /**
     * PortalsSdk Constructor.
     *
     * @param RequestService $requestService The request service
     */
    public function __construct(private readonly RequestService $requestService)
    {
        $this->datasetRepository      = null;
        $this->distributionRepository = null;
    }

    public function datasets(): DatasetRepository
    {
        if (null === $this->datasetRepository) {
            $this->datasetRepository = new DatasetRepository($this->requestService);
        }

        return $this->datasetRepository;
    }

    public function distributions(): DistributionRepository
    {
        if (null === $this->distributionRepository) {
            $this->distributionRepository = new DistributionRepository($this->requestService);
        }

        return $this->distributionRepository;
    }
}
