<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/portals-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PortalsSdk\Repositories;

use XpertSelect\PortalsSdk\RequestService;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class DistributionRepository.
 */
class DistributionRepository
{
    /**
     * DistributionRepository constructor.
     *
     * @param RequestService $requestService The request service
     */
    public function __construct(protected RequestService $requestService)
    {
    }

    /**
     * Execute an HTTP GET request to fetch a distribution in the Portals API.
     *
     * @param string $dataset_id The unique identifier of the dataset
     * @param string $id         The unique identifier of the Dataset
     *
     * @return array<string, mixed> The response from the Portals API
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function get(string $dataset_id, string $id): array
    {
        $response = $this->requestService->get($this->buildPath($dataset_id, $id));

        if ($response->hasStatus(200) && $response->hasValidJson('distribution-get.json')) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Execute an HTTP GET request to fetch a list of distributions from a specific dataset in the
     * Portals API.
     *
     * @param string $dataset_id The unique identifier of the dataset
     *
     * @return array<string, mixed> The response from the Portals API
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function index(string $dataset_id, int $start = 0): array
    {
        $response = $this->requestService->get($this->buildPath($dataset_id), ['start' => $start]);

        if ($response->hasStatus(200) && $response->hasValidJson('distribution-index.json')) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Build the path for the request.
     *
     * @param string      $dataset_id      The unique identifier of a dataset
     * @param null|string $distribution_id The unique identifier of a distribution
     *
     * @return string The path
     */
    private function buildPath(string $dataset_id, ?string $distribution_id = null): string
    {
        $path = sprintf('api/datasets/%s/distributions', $dataset_id);

        return null === $distribution_id ? $path : sprintf('%s/%s', $path, $distribution_id);
    }
}
