<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/portals-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PortalsSdk\Repositories;

use XpertSelect\PortalsSdk\RequestService;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class DatasetRepository.
 */
class DatasetRepository
{
    /**
     * DatasetRepository constructor.
     *
     * @param RequestService $requestService The request service
     */
    public function __construct(protected RequestService $requestService)
    {
    }

    /**
     * Execute an HTTP GET request to fetch a dataset in the Portals API.
     *
     * @param string $id The unique identifier of the Dataset
     *
     * @return array<string, mixed> The response from the Portals API
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function get(string $id): array
    {
        $response = $this->requestService->get($this->buildPath($id));

        if ($response->hasStatus(200) && $response->hasValidJson('dataset-get.json')) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Execute an HTTP GET request to fetch a list of datasets in the Portals API.
     *
     * @return array<string, mixed> The response from the Portals API
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function index(int $start = 0): array
    {
        $response = $this->requestService->get($this->buildPath(), ['start' => $start]);

        if ($response->hasStatus(200) && $response->hasValidJson('dataset-index.json')) {
            return $response->json(true);
        }

        throw new ResponseException($response);
    }

    /**
     * Get a list of all dataset IDs from the Portals API.
     *
     * @return array<int, string> A list with all dataset ids
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function listDatasetIds(): array
    {
        $datasetIds = [];
        $start      = 0;

        do {
            $response = $this->index($start);
            $total    = $response['meta']['total'];
            $start   += $response['meta']['rows'];
            $datasetIds = array_merge($datasetIds, array_map(function($dataset) {
                return $dataset['id'];
            }, $response['data']));
        } while ($start < $total);

        return $datasetIds;
    }

    /**
     * Get all datasets from the Portal.
     *
     * @return array<int, array<string, mixed>> A list of datasets
     *
     * @throws ClientException   Thrown when the request could not be sent
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     */
    public function all(): array
    {
        $ids = $this->listDatasetIds();

        $datasets = [];

        foreach ($ids as $id) {
            $datasets[] = $this->get($id);
        }

        return $datasets;
    }

    /**
     * Build the path for the request.
     *
     * @param ?string $dataset_id The unique identifier of a dataset
     *
     * @return string The path
     */
    private function buildPath(?string $dataset_id = null): string
    {
        $path = 'api/datasets';

        return null === $dataset_id ? $path : sprintf('%s/%s', $path, $dataset_id);
    }
}
