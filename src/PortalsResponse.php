<?php

/**
 * This file is part of the xpertselect/portals-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PortalsSdk;

use XpertSelect\PsrTools\PsrResponse;

/**
 * Class PortalsResponse.
 */
class PortalsResponse extends PsrResponse
{
    /**
     * {@inheritdoc}
     */
    public function getJsonSchemaPath(): string
    {
        return __DIR__ . '/../resources/json-schemas/';
    }
}
