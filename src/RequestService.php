<?php

/**
 * This file is part of the xpertselect/portals-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PortalsSdk;

use Psr\Http\Message\ResponseInterface;
use XpertSelect\PsrTools\HttpRequestService as RS;
use XpertSelect\PsrTools\PsrResponse;

class RequestService extends RS
{
    /**
     * {@inheritdoc}
     */
    public function createUserAgent(): string
    {
        return 'xpertselect/portals-sdk';
    }

    /**
     * {@inheritdoc}
     */
    public function getPsrResponse(ResponseInterface $response): PsrResponse
    {
        return new PortalsResponse($response);
    }
}
