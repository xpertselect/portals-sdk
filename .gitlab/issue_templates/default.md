## Summary

Summarize the bug encountered concisely.

## Version

What version of `xpertselect/portals-sdk` are you using?

```shell
composer show xpertselect/portals-sdk 2>&1 | grep versions
```
