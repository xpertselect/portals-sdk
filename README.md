# XpertSelect / Portals SDK

[gitlab.com/xpertselect/portals-sdk](https://gitlab.com/xpertselect/portals-sdk)

The PHP-SDK for interacting with the JSON API of an XpertSelect Portals installation.

## License

View the `LICENSE.md` file for licensing details.
