<?php

/**
 * This file is part of the xpertselect/portals-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\TestCase;
use XpertSelect\PortalsSdk\Repositories\DatasetRepository;
use XpertSelect\PortalsSdk\RequestService;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * @internal
 */
class DatasetTest extends TestCase
{
    public static function validIndexFixtures(): array
    {
        return [
            ['portals-response/dataset-index-valid.1.json'],
            ['portals-response/dataset-index-valid.2.json'],
        ];
    }

    public static function invalidIndexFixtures(): array
    {
        return [
            ['portals-response/dataset-index-invalid.1.json'],
            ['portals-response/dataset-index-invalid.2.json'],
        ];
    }

    public static function validGetFixtures(): array
    {
        return [
            ['portals-response/dataset-get-valid.json'],
        ];
    }

    public static function invalidGetFixtures(): array
    {
        return [
            ['portals-response/dataset-get-invalid.json'],
        ];
    }

    /**
     * @dataProvider validIndexFixtures
     */
    public function testResponseValidatesAgainstJsonScheme(string $fixture): void
    {
        try {
            $requestService = M::mock(RequestService::class, function(MI $mock) use ($fixture) {
                $mock->shouldReceive('get')
                    ->with('api/datasets', ['start' => 0])
                    ->andReturn($this->createMockedResponse(200, $fixture));
            })->makePartial();

            $repository = new DatasetRepository($requestService);
            $response   = $repository->index(0);
            Assert::assertIsArray($response);

            Assert::assertArrayHasKey('jsonapi', $response);
            $jsonapi = $response['jsonapi'] ?? [];
            Assert::assertIsArray($jsonapi);
            Assert::assertArrayHasKey('version', $jsonapi);

            Assert::assertArrayHasKey('meta', $response);
            $meta = $response['meta'] ?? [];
            Assert::assertIsArray($meta);
            Assert::assertArrayHasKey('start', $meta);
            Assert::assertArrayHasKey('rows', $meta);
            Assert::assertArrayHasKey('total', $meta);

            Assert::assertArrayHasKey('data', $response);

            foreach ($response['data'] as $data) {
                Assert::assertIsArray($data);
                Assert::assertArrayHasKey('type', $data);
                Assert::assertArrayHasKey('id', $data);

                Assert::assertArrayHasKey('attributes', $data);
                $attributes = $data['attributes'] ?? [];
                Assert::assertIsArray($attributes);
                Assert::assertArrayHasKey('dct:title', $attributes);

                Assert::assertArrayHasKey('meta', $data);
                $meta = $data['meta'] ?? [];
                Assert::assertIsArray($meta);
                Assert::assertArrayHasKey('visibility', $meta);

                Assert::assertArrayHasKey('links', $data);
                $links = $data['links'] ?? [];
                Assert::assertIsArray($links);
                Assert::assertArrayHasKey('self', $links);
            }
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     * @dataProvider invalidIndexFixtures
     */
    public function testInvalidResponseDoesNotValidateAgainstJsonScheme(string $fixture): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(RequestService::class, function(MI $mock) use ($fixture) {
                $mock->shouldReceive('get')
                    ->with('api/datasets', ['start' => 0])
                    ->andReturn($this->createMockedResponse(200, $fixture));
            });

            $repository = new DatasetRepository($requestService);
            $repository->index();
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     * @dataProvider validGetFixtures
     */
    public function testValidGetResponseDoesValidateAgainstJsonScheme(string $fixture): void
    {
        try {
            $id = 'luchtfoto-2022';

            $requestService = M::mock(RequestService::class, function(MI $mock) use ($id, $fixture) {
                $mock->shouldReceive('get')
                    ->with('api/datasets/' . $id)
                    ->andReturn($this->createMockedResponse(200, $fixture));
            });

            $repository = new DatasetRepository($requestService);
            $response   = $repository->get($id);
            Assert::assertIsArray($response);

            Assert::assertArrayHasKey('jsonapi', $response);
            $jsonapi = $response['jsonapi'] ?? [];
            Assert::assertIsArray($jsonapi);
            Assert::assertArrayHasKey('version', $jsonapi);

            Assert::assertArrayHasKey('data', $response);

            $data = $response['data'] ?? [];
            Assert::assertIsArray($data);
            Assert::assertArrayHasKey('type', $data);
            Assert::assertArrayHasKey('id', $data);

            Assert::assertArrayHasKey('attributes', $data);
            $attributes = $data['attributes'] ?? [];
            Assert::assertIsArray($attributes);
            Assert::assertArrayHasKey('dct:title', $attributes);
            Assert::assertArrayHasKey('dct:description', $attributes);
            Assert::assertArrayHasKey('dcat:theme', $attributes);
            Assert::assertArrayHasKey('donl:datasetStatus', $attributes);
            Assert::assertArrayHasKey('dct:accessRights', $attributes);

            Assert::assertArrayHasKey('meta', $data);
            $meta = $data['meta'] ?? [];
            Assert::assertIsArray($meta);
            Assert::assertArrayHasKey('visibility', $meta);
            Assert::assertArrayHasKey('maintainer', $meta);

            Assert::assertArrayHasKey('links', $data);
            $links = $data['links'] ?? [];
            Assert::assertIsArray($links);
            Assert::assertArrayHasKey('self', $links);

            Assert::assertArrayHasKey('relationships', $data);
            $relationships = $data['relationships'] ?? [];
            foreach ($relationships as $relationship) {
                Assert::assertIsArray($relationship);
                Assert::assertArrayHasKey('data', $relationship);

                $relationshipData = $relationship['data'];
                Assert::assertIsArray($relationshipData);
                Assert::assertArrayHasKey('type', $relationshipData);
                Assert::assertArrayHasKey('id', $relationshipData);
            }
            Assert::assertArrayHasKey('included', $response);

            $includeds = $response['included'] ?? [];
            Assert::assertIsArray($includeds);
            foreach ($includeds as $included) {
                Assert::assertIsArray($included);
                Assert::assertArrayHasKey('type', $included);
                Assert::assertArrayHasKey('id', $included);

                Assert::assertArrayHasKey('attributes', $included);
                $includedAttributes = $included['attributes'] ?? [];
                Assert::assertIsArray($includedAttributes);
                Assert::assertArrayHasKey('dct:title', $includedAttributes);
                Assert::assertArrayHasKey('dct:description', $includedAttributes);
                Assert::assertArrayHasKey('dct:format', $includedAttributes);
                Assert::assertArrayHasKey('dct:license', $includedAttributes);
                Assert::assertArrayHasKey('dcat:accessURL', $includedAttributes);
            }
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     * @dataProvider invalidGetFixtures
     */
    public function testInvalidGetResponseDoesValidateAgainstJsonScheme($fixture): void
    {
        try {
            $this->expectException(ResponseException::class);
            $id = 'luchtfoto-2022';

            $requestService = M::mock(RequestService::class, function(MI $mock) use ($id, $fixture) {
                $mock->shouldReceive('get')
                    ->with('api/datasets/' . $id)
                    ->andReturn($this->createMockedResponse(200, $fixture));
            });

            $repository = new DatasetRepository($requestService);
            $repository->get($id);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testListDatasetIds(): void
    {
        try {
            $requestService = M::mock(RequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/datasets', ['start' => 0])
                    ->andReturn($this->createMockedResponse(200, 'portals-response/dataset-index-valid.1.json'));
                $mock->shouldReceive('get')
                    ->with('api/datasets', ['start' => 2])
                    ->andReturn($this->createMockedResponse(200, 'portals-response/dataset-index-valid.2.json'));
            });

            $repository = new DatasetRepository($requestService);
            $response   = $repository->listDatasetIds();

            Assert::assertIsArray($response);
            Assert::assertCount(4, $response);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testListAllDatasets(): void
    {
        try {
            $ids = [
                'some-name',
                'some-name-2',
                'some-name-3',
                'some-name-4',
            ];

            $requestService = M::mock(RequestService::class, function(MI $mock) use ($ids) {
                $mock->shouldReceive('get')
                    ->with('api/datasets', ['start' => 0])
                    ->andReturn($this->createMockedResponse(200, 'portals-response/dataset-index-valid.1.json'));
                $mock->shouldReceive('get')
                    ->with('api/datasets', ['start' => 2])
                    ->andReturn($this->createMockedResponse(200, 'portals-response/dataset-index-valid.2.json'));
                foreach ($ids as $id) {
                    $mock->shouldReceive('get')
                        ->with('api/datasets/' . $id)
                        ->andReturn($this->createMockedResponse(200, 'portals-response/dataset-get-valid.json'));
                }
            });

            $repo     = new DatasetRepository($requestService);
            $response = $repo->all();

            Assert::assertIsArray($response);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }
}
