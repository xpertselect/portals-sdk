<?php

/**
 * This file is part of the xpertselect/portals-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\TestCase;
use XpertSelect\PortalsSdk\Repositories\DistributionRepository;
use XpertSelect\PortalsSdk\RequestService;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * @internal
 */
class DistributionTest extends TestCase
{
    public static function validIndexFixtures(): array
    {
        return [
            ['portals-response/distribution-index-valid.json'],
        ];
    }

    public static function invalidIndexFixtures(): array
    {
        return [
            ['portals-response/distribution-index-invalid.1.json'],
        ];
    }

    public static function validGetFixtures(): array
    {
        return [
            ['portals-response/distribution-get-valid.json'],
        ];
    }

    public static function invalidGetFixtures(): array
    {
        return [
            ['portals-response/distribution-get-invalid.json'],
        ];
    }

    /**
     * @dataProvider validIndexFixtures
     */
    public function testValidIndexResponseValidatesAgainstJsonSchema(string $fixture): void
    {
        try {
            $dataset_id = 'abc';

            $requestService = M::mock(RequestService::class, function(MI $mock) use ($dataset_id, $fixture) {
                $mock->shouldReceive('get')
                    ->with(sprintf('api/datasets/%s/distributions', $dataset_id), ['start' => 0])
                    ->andReturn($this->createMockedResponse(200, $fixture));
            });

            $repository = new DistributionRepository($requestService);
            $response   = $repository->index($dataset_id);
            Assert::assertIsArray($response);

            Assert::assertArrayHasKey('jsonapi', $response);
            $jsonapi = $response['jsonapi'] ?? [];
            Assert::assertIsArray($jsonapi);
            Assert::assertArrayHasKey('version', $jsonapi);

            Assert::assertArrayHasKey('data', $response);

            $dataObjects = $response['data'] ?? [];
            Assert::assertIsArray($dataObjects);
            foreach ($dataObjects as $data) {
                Assert::assertArrayHasKey('type', $data);
                Assert::assertArrayHasKey('id', $data);

                Assert::assertArrayHasKey('attributes', $data);
                $attributes = $data['attributes'] ?? [];
                Assert::assertIsArray($attributes);
                Assert::assertArrayHasKey('id', $attributes);
                Assert::assertArrayHasKey('dcat:accessURL', $attributes);
                Assert::assertArrayHasKey('dct:title', $attributes);
                Assert::assertArrayHasKey('dct:description', $attributes);
                Assert::assertArrayHasKey('dct:format', $attributes);
                Assert::assertArrayHasKey('dct:license', $attributes);
                Assert::assertArrayHasKey('dct:issued', $attributes);
                Assert::assertArrayHasKey('dct:language', $attributes);
                Assert::assertArrayHasKey('donl:distributionType', $attributes);
                Assert::assertArrayHasKey('links', $data);
                $links = $data['links'] ?? [];
                Assert::assertIsArray($links);
                Assert::assertArrayHasKey('self', $links);
            }
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     * @dataProvider invalidIndexFixtures
     */
    public function testInvalidIndexResponseValidatesAgainstJsonSchema(string $fixture): void
    {
        try {
            $this->expectException(ResponseException::class);
            $dataset_id = 'abc';

            $requestService = M::mock(RequestService::class, function(MI $mock) use ($dataset_id, $fixture) {
                $mock->shouldReceive('get')
                    ->with(sprintf('api/datasets/%s/distributions', $dataset_id), ['start' => 0])
                    ->andReturn($this->createMockedResponse(200, $fixture));
            });

            $repository = new DistributionRepository($requestService);
            $repository->index($dataset_id);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     * @dataProvider validGetFixtures
     */
    public function testValidGetResponseValidatesAgainstJsonSchema(string $fixture): void
    {
        try {
            $dataset_id      = 'abc';
            $distribution_id = 'def';

            $requestService = M::mock(RequestService::class, function(MI $mock) use ($dataset_id, $distribution_id, $fixture) {
                $mock->shouldReceive('get')
                    ->with(sprintf('api/datasets/%s/distributions/%s', $dataset_id, $distribution_id))
                    ->andReturn($this->createMockedResponse(200, $fixture));
            });

            $repository = new DistributionRepository($requestService);
            $response   = $repository->get($dataset_id, $distribution_id);
            Assert::assertIsArray($response);

            Assert::assertArrayHasKey('jsonapi', $response);
            $jsonapi = $response['jsonapi'] ?? [];
            Assert::assertIsArray($jsonapi);
            Assert::assertArrayHasKey('version', $jsonapi);

            Assert::assertArrayHasKey('data', $response);

            $data = $response['data'] ?? [];
            Assert::assertIsArray($data);
            Assert::assertArrayHasKey('type', $data);
            Assert::assertArrayHasKey('id', $data);

            Assert::assertArrayHasKey('attributes', $data);
            $attributes = $data['attributes'] ?? [];
            Assert::assertIsArray($attributes);
            Assert::assertArrayHasKey('id', $attributes);
            Assert::assertArrayHasKey('dcat:accessURL', $attributes);
            Assert::assertArrayHasKey('dct:title', $attributes);
            Assert::assertArrayHasKey('dct:description', $attributes);
            Assert::assertArrayHasKey('dct:format', $attributes);
            Assert::assertArrayHasKey('dct:license', $attributes);
            Assert::assertArrayHasKey('dct:issued', $attributes);
            Assert::assertArrayHasKey('dct:language', $attributes);
            Assert::assertArrayHasKey('donl:distributionType', $attributes);
            Assert::assertArrayHasKey('links', $data);
            $links = $data['links'] ?? [];
            Assert::assertIsArray($links);
            Assert::assertArrayHasKey('self', $links);
        } catch (ClientException|ResponseException $e) {
            $this->fail($e->getMessage());
        }
    }

    /**
     * @dataProvider invalidGetFixtures
     */
    public function testInvalidGetResponseValidatesAgainstJsonSchema($fixture): void
    {
        try {
            $this->expectException(ResponseException::class);
            $dataset_id      = 'abc';
            $distribution_id = 'def';

            $requestService = M::mock(RequestService::class, function(MI $mock) use ($dataset_id, $distribution_id, $fixture) {
                $mock->shouldReceive('get')
                    ->with(sprintf('api/datasets/%s/distributions/%s', $dataset_id, $distribution_id))
                    ->andReturn($this->createMockedResponse(200, $fixture));
            });

            $repository = new DistributionRepository($requestService);
            $repository->get($dataset_id, $distribution_id);
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }
}
