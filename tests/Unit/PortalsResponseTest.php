<?php

/**
 * This file is part of the xpertselect/portals-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Tests\TestCase;
use XpertSelect\PortalsSdk\PortalsResponse;

/**
 * @internal
 */
class PortalsResponseTest extends TestCase
{
    public function testGetJsonSchemaPath(): void
    {
        $psrResponse = M::mock(ResponseInterface::class, function(MI $mock) {
            $stream = M::mock(StreamInterface::class, function(MI $mock) {
            });

            $mock->shouldReceive('getStatusCode')->andReturn(200);
            $mock->shouldReceive('getBody')->andReturn($stream);
        });

        $response = new PortalsResponse($psrResponse);

        Assert::assertStringContainsString('/../resources/json-schemas/', $response->getJsonSchemaPath());
    }
}
