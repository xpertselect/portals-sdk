<?php

/**
 * This file is part of the xpertselect/portals-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Mockery as M;
use Tests\TestCase;
use XpertSelect\PortalsSdk\PortalsSdk;
use XpertSelect\PortalsSdk\RequestService;

/**
 * @internal
 */
class PortalsSdkTest extends TestCase
{
    public function testSameRepositoriesAreReturnedEveryTime(): void
    {
        $requestService = M::mock(RequestService::class);
        $sdk            = new PortalsSdk($requestService);

        $this->assertSame($sdk->datasets(), $sdk->datasets());
        $this->assertSame($sdk->distributions(), $sdk->distributions());
    }
}
